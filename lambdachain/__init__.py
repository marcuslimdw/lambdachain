from lambdachain.lambda_chain import LambdaChain as LC
from lambdachain.lambda_identifier import Lambda as _

__all__ = ['LC', '_']
